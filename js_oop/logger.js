const EventEmitter = require('events');
const { v4 } = require('uuid');

class Logger extends EventEmitter {
    log(msg) {
        // Call event
        this.emit('message', { id: v4(), msg });
    }
}

module.exports = Logger;