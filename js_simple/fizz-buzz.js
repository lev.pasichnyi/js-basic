// first and second part of task
const END_CONDITION = 101;
// FOR TEST: if 'true', we are doing second part of a task
const isFIZZBUZZ = true; 
let counter = 1;

while (counter <  END_CONDITION) {

// FIZZBUZZ for all cases for dividing on 3 or 5
if (isFIZZBUZZ && counter%3 === 0 && counter%5 === 0) {
    console.log('FizzBuzz');
    counter++;
    continue;
}     
// 'Fizz' if dividing on 3 or 'Buzz', if dividing on 5
if (!isFIZZBUZZ && counter%3 === 0) {
    console.log('Fizz');
    counter++;
    continue;
} else if (!isFIZZBUZZ && counter%5 === 0) {
    console.log('Buzz');
    counter++;
    continue;
}
// other cases of numbers
   console.log(counter++);
}
