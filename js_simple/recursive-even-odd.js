// isEven recursion function, but better way is return n % 2 === 0 ;)
const isEven = function(value){
    // fix for negative numbers
    if (value < 0) value = Math.abs(value);
        if (value === 0) return true;
            if (value === 1) return false;
    return isEven(value - 2);
    }
    // test for positive numbers
    console.log(isEven(50));
    console.log(isEven(23));
    console.log(isEven(0));
    
    // for negative numbers
    console.log(isEven(-1));
    console.log(isEven(-20));