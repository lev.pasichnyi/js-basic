function loginUser(email, password) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log("Now we have an email and a password");
            resolve({ userEmail: email, userPassword: password });
        }, 3000);
    })
}

function getBigThing(someString) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log("Now we have some String");
            resolve([someString, '  ', 'hello', 'bye']);
        }, 200);
    })
}

function getTinyDetails(aWord) {
    return new Promise(resolve => {
        setTimeout(() => {
            console.log("Now we have a word");
            resolve(`${aWord} and something ${aWord}`);
        }, 500);
    })
}

async function doSomething() {
    try {
        const logUser = await loginUser('Peter', 43252);
        const bigThing = await getBigThing(logUser.userEmail)
        const tinyDetails = await getTinyDetails(bigThing[2])
        console.log(tinyDetails)
    } catch (err) {
        console.log("Something goes wrong")
    }
}

doSomething()