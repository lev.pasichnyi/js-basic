function range(min, max, step = 1) {
    // simple swap
    if (min > max) {
        let temp = min;
        min = max;
        max = temp;
    }

    // array init, variables init
    let arr = [];
    let stepCounter = min;
    let nextStep = Math.abs(step);
    // total number of values for print
    let total = (max - min) + 1;

    for (let key = 0; key < total / nextStep; key++ , stepCounter += nextStep) {
        arr[key] = stepCounter;
    }
    // step < 0 then use as reverse flag
    if (step < 0) {
        let reverse = [];
        for (let key in arr) {
            reverse.unshift(arr[key]);
        }
        return reverse;
    }
    return arr;
}
function sum(arr) {
    let sum = 0;
    for (let key in arr) sum += arr[key];
    return sum;
}

// test cases
console.log(range(1, 10));
console.log(sum(range(1, 10)));
console.log(range(5, 2, -1));
console.log(sum(range(5, 2, -1)));
console.log(range(0, 8, 2));
